package ir.sass.baseui.view
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception


class MotherAdapter<DB : ViewDataBinding , DataType> (
    private val wrapper: RecyclerItemWrapper<DB,DataType>
) : RecyclerView.Adapter<MotherAdapter.MotherAdapterVH<DB>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MotherAdapterVH<DB> =
        MotherAdapterVH(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), wrapper.layout, parent, false)
        )



    override fun onBindViewHolder(
        holder: MotherAdapterVH<DB>,
        position: Int
    ) {
        wrapper.bindingFun.invoke(holder.binding,wrapper.list[position],position)
        if(position != 0 && position == wrapper.list.size-1){
            wrapper.lastItem.invoke()
        }
    }

    override fun getItemCount(): Int = wrapper.list.size


    fun changeList(list: List<DataType>){
        val diffCallback = DiffCallback(wrapper.list, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        wrapper.list.clear()
        wrapper.list.addAll(list)
        diffResult.dispatchUpdatesTo(this)
    }

    fun clearList(){
        wrapper.list.clear()
        notifyDataSetChanged()
    }

    fun changeRangeList(list: List<DataType>,start : Int,end : Int){
        try {
            val oldSize = wrapper.list.size
            if(wrapper.list.isEmpty()){
                changeList(list)
            }else{
                if(oldSize < start){
                    Log.e("Error","out of size")
                }else{
                    val newList = mutableListOf<DataType>()
                    if(oldSize == start){

                        newList.addAll(wrapper.list)
                        newList.addAll(list)
                        wrapper.list.clear()
                        wrapper.list.addAll(newList)
                        notifyItemRangeInserted(start,end-start)
                    }else if(oldSize <= end){

                        newList.addAll(wrapper.list.subList(0,start))
                        newList.addAll(list)
                        wrapper.list.clear()
                        wrapper.list.addAll(newList)
                        notifyItemRangeChanged(start,oldSize)
                        if(end-oldSize != 0)
                            notifyItemRangeInserted(oldSize,end-oldSize)
                    }else {

                        newList.addAll(wrapper.list.subList(0,start))
                        newList.addAll(list)
                        newList.addAll(wrapper.list.subList(end,wrapper.list.size))
                        wrapper.list.clear()
                        wrapper.list.addAll(newList)
                        notifyItemRangeChanged(start,end-start)
                    }
                }
            }
        }catch (e : Exception){
        }
    }


    override fun getItemId(position: Int): Long = position.toLong()

    class DiffCallback<DataType>(private val old : List<DataType>,private val new : List<DataType>) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = old.size

        override fun getNewListSize(): Int = new.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            old[oldItemPosition] === new[newItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            old[oldItemPosition] == new[newItemPosition]


    }

    class MotherAdapterVH<DB : ViewDataBinding>(var binding : DB) : RecyclerView.ViewHolder(binding.root)

}

// this wrapper include layout and list, it also include a lambda for binding


data class RecyclerItemWrapper<DB : ViewDataBinding, DataType>(
    @LayoutRes
    var layout : Int,
    val bindingFun : (binding : DB,item : DataType,pos : Int) -> Unit,
    val lastItem : () -> Unit
){
    internal val list = mutableListOf<DataType>()
}

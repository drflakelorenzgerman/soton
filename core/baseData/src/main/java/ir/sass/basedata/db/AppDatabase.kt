package ir.sass.basedata.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.sass.basedata.db.model.LastPosModelEntity
import ir.sass.basedata.db.model.NearByLocResultModelEntity
import ir.sass.basedata.db.model.NearByLocSummeryModelEntity

@Database(entities = [NearByLocResultModelEntity::class, LastPosModelEntity::class, NearByLocSummeryModelEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun nearbyLocDao() : NearbyLocDao
}
const val DATA_BASE_NAME ="data_base"

const val TABLE_NEAR_BY_LOC_RESULT = "table_near_by_loc_result"
const val TABLE_LAST_POS = "table_last_pos"
const val TABLE_NEAR_BY_LOC_SUMMERY = "table_near_by_loc_summery"

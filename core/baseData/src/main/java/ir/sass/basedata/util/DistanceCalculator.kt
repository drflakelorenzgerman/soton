package ir.sass.basedata.util

import android.location.Location

fun diffInTwoPos(firstLat : Double,secondLat : Double,firstLon : Double,secondLon : Double) : Float{
    val results = FloatArray(1)
    Location.distanceBetween(firstLat,firstLon,secondLat,secondLon,results)
    return results[0]
}
package ir.sass.basedata.di

import android.app.Application
import android.util.Log
import androidx.room.Room
import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.logger.Logger
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.sass.basedata.db.AppDatabase
import ir.sass.basedata.db.DATA_BASE_NAME
import ir.sass.basedata.db.NearbyLocDao
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object BaseDataModule {
    @Provides
    fun provideRetrofit(httpClient : OkHttpClient) : Retrofit = Retrofit.Builder()
        .baseUrl("https://api.tomtom.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient)
        .build()

    @Provides
    fun provideOkHttpClient() : OkHttpClient{
        val okClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                var request: Request = chain.request()
                val url: HttpUrl = request.url.newBuilder()
                    .addQueryParameter("key", "6jyO4FdG2hA7jsQgTk6B6G0zdqGKAw9A")
                    .build()
                request = request.newBuilder().url(url).build()
                chain.proceed(request)
            }.addInterceptor(CurlInterceptor(object : Logger {
                override fun log(message: String) {
                    Log.v("Ok2Curl", message)
                }
            }))
            .build()
        return okClient
    }

    @Singleton
    @Provides
    fun provideDao(applicationContext: Application) : NearbyLocDao{
        val db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, DATA_BASE_NAME
        ).build()
        return db.nearbyLocDao()
    }
}


package ir.sass.basedata.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ir.sass.basedata.db.TABLE_LAST_POS

@Entity(tableName = TABLE_LAST_POS)
data class LastPosModelEntity(
    @ColumnInfo(name  ="lat")
    val lat: Double,
    @ColumnInfo(name  ="lon")
    val lon: Double,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name  ="id")
    val id : Int = 0
)
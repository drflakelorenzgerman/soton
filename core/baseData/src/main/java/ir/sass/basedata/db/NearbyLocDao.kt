package ir.sass.basedata.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ir.sass.basedata.db.model.LastPosModelEntity
import ir.sass.basedata.db.model.NearByLocResultModelEntity
import ir.sass.basedata.db.model.NearByLocSummeryModelEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NearbyLocDao {
    @Query("SELECT * FROM ${TABLE_NEAR_BY_LOC_RESULT} LIMIT :limit OFFSET :offset")
    fun getRangeOfNearByLocResult(limit : Int,offset : Int) : Flow<List<NearByLocResultModelEntity>>

    @Query("SELECT * FROM ${TABLE_NEAR_BY_LOC_RESULT}")
    fun getAllNearByLocResult() : Flow<List<NearByLocResultModelEntity>>

    @Query("DELETE FROM ${TABLE_NEAR_BY_LOC_RESULT}")
    fun deleteNearByLocResult()

    @Insert
    fun insertNearByLocResults(list : List<NearByLocResultModelEntity>)

    @Query("SELECT * FROM ${TABLE_LAST_POS} LIMIT 1")
    fun getLastLocation() : LastPosModelEntity?

    @Query("DELETE FROM ${TABLE_LAST_POS}")
    fun deleteLastLocation()

    @Insert
    fun insertLastLocation(loc : LastPosModelEntity)

    @Query("SELECT * FROM ${TABLE_NEAR_BY_LOC_SUMMERY} LIMIT 1")
    fun getSummery() : NearByLocSummeryModelEntity?

    @Query("DELETE FROM ${TABLE_NEAR_BY_LOC_SUMMERY}")
    fun deleteSummrey()

    @Insert
    fun insertSummery(summery : NearByLocSummeryModelEntity)
}
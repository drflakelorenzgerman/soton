package ir.sass.basedata.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ir.sass.basedata.db.TABLE_NEAR_BY_LOC_RESULT

@Entity(tableName = TABLE_NEAR_BY_LOC_RESULT)
data class NearByLocResultModelEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name  ="id")
    val id : Int = 0,
    @ColumnInfo(name  ="results")
    val results: String
)



package ir.sass.domain.model

data class NearByLocModel(
    val results: List<Result>,
    val summary: Summary? = null,
    val limit: Int = 0,
    val ofs: Int = 0
)

data class Result(
    val address: Address?,
    val dist: Double?,
    val entryPoints: List<EntryPoint>?,
    val id: String?,
    val info: String?,
    val poi: Poi?,
    val position: Pos?,
    val score: Double?,
    val type: String?,
    val viewport: Viewport?
)

data class Summary(
    val fuzzyLevel: Int?,
    val geoBias: Pos?,
    val numResults: Int?,
    val offset: Int?,
    val queryTime: Int?,
    val queryType: String?,
    val totalResults: Int?
)

data class Address(
    val country: String?,
    val countryCode: String?,
    val countryCodeISO3: String?,
    val countrySecondarySubdivision: String?,
    val countrySubdivision: String?,
    val freeformAddress: String?,
    val localName: String?,
    val municipality: String?,
    val postalCode: String?,
    val streetName: String?,
    val streetNumber: String?
)

data class EntryPoint(
    val position: Pos?,
    val type: String?
)

data class Poi(
    val categories: List<String>?,
    val categorySet: List<CategorySet>?,
    val classifications: List<Classification>?,
    var name: String?
)


data class Viewport(
    val btmRightPoint: Pos?,
    val topLeftPoint: Pos?
)

data class CategorySet(
    val id: Int?
)

data class Classification(
    val code: String?,
    val names: List<Name>?
)

data class Name(
    val name: String?,
    val nameLocale: String?
)


data class Pos(
    val lat: Double?,
    val lon: Double?
)
package ir.sass.domain.repository

import ir.sass.basedomain.model.Domain
import ir.sass.domain.model.NearByLocModel
import kotlinx.coroutines.flow.Flow

interface NearbyLocRepository {
    fun nearbySearch(lat : Double, lon : Double, radius : Int, limit : Int, ofs : Int) : Flow<Domain<NearByLocModel>>

    fun nearbySearchRemote(lat : Double, lon : Double, radius : Int, limit : Int, ofs : Int) : Flow<Domain<NearByLocModel>>



}
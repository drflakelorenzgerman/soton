package ir.sass.domain.usecase

import ir.sass.basedomain.model.Domain
import ir.sass.basedomain.useCase.MotherUseCase
import ir.sass.domain.model.NearByLocModel
import ir.sass.domain.repository.NearbyLocRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetNearByLocFromLocalUseCase @Inject constructor(
    val nearbyLocRepository: NearbyLocRepository
) : MotherUseCase<GetNearByLocFromLocalUseCase.GetNearByLocFromLocalUseCaseParam, NearByLocModel>() {
    class GetNearByLocFromLocalUseCaseParam(
        val lat : Double,
        val lon : Double,
        val radius : Int,
        val limit : Int,
        val ofs : Int,
    )

    override fun invoke(input: GetNearByLocFromLocalUseCaseParam): Flow<Domain<NearByLocModel>> =
        nearbyLocRepository.nearbySearch(input.lat,input.lon,input.radius,input.limit,input.ofs)
}
package ir.sass.domain.usecase

import ir.sass.basedomain.model.Domain
import ir.sass.basedomain.useCase.MotherUseCase
import ir.sass.domain.model.NearByLocModel
import ir.sass.domain.repository.NearbyLocRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetNearByLocFromRemoteUseCase @Inject constructor(
    val nearbyLocRepository: NearbyLocRepository
) : MotherUseCase<GetNearByLocFromRemoteUseCase.GetNearByLocFromRemoteUseCaseParam, NearByLocModel>() {
    class GetNearByLocFromRemoteUseCaseParam(
        val lat : Double,
        val lon : Double,
        val radius : Int,
        val limit : Int,
        val ofs : Int,
    )

    override fun invoke(input: GetNearByLocFromRemoteUseCaseParam): Flow<Domain<NearByLocModel>> =
        nearbyLocRepository.nearbySearchRemote(input.lat,input.lon,input.radius,input.limit,input.ofs)
}
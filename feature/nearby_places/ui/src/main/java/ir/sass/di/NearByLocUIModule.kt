package ir.sass.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import ir.sass.domain.repository.NearbyLocRepository
import ir.sass.domain.usecase.GetNearByLocFromRemoteUseCase

@Module
@InstallIn(FragmentComponent::class)
class NearByLocUIModule {
    @Provides
    fun provideGetNearByLocUseCase(
        repository: NearbyLocRepository
    ) : GetNearByLocFromRemoteUseCase = GetNearByLocFromRemoteUseCase(repository)
}
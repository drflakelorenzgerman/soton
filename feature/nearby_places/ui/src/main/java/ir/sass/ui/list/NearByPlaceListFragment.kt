package ir.sass.ui.list

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.gms.location.FusedLocationProviderClient
import dagger.hilt.android.AndroidEntryPoint
import ir.sass.basedata.model.toJsonString
import ir.sass.basedata.util.diffInTwoPos
import ir.sass.baseui.util.getPermissions
import ir.sass.baseui.util.toast
import ir.sass.baseui.view.MotherAdapter
import ir.sass.baseui.view.MotherFragment
import ir.sass.baseui.view.RecyclerItemWrapper
import ir.sass.domain.model.Result
import ir.sass.ui.R
import ir.sass.ui.databinding.FragmentNearByPlaceListBinding
import ir.sass.ui.databinding.ItemNearByLocationBinding
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class NearByPlaceListFragment : MotherFragment<FragmentNearByPlaceListBinding>(
    MotherFragmentSetting(
        R.layout.fragment_near_by_place_list,
        "List"
    )
) {
    override val viewModel: NearByPlaceListFragmentViewModel by viewModels()
    var mFusedLocationClient: FusedLocationProviderClient? = null
    var ofs = 0
    var lat = 0.0
    var lon = 0.0
    var loading = false
    val adapter = MotherAdapter<ItemNearByLocationBinding,Result>(
        RecyclerItemWrapper(R.layout.item_near_by_location,bindingFun = { binding, item, pos ->
            binding.item = item.apply {
                item.poi?.name = item.poi?.name
            }
            binding.navigate = {
                findNavController().navigate(NearByPlaceListFragmentDirections.actionNearByPlaceListFragmentToNearByPlaceDetailFragment(
                    toJsonString(item)
                ))
            }
        },lastItem = {
            ofs += viewModel.limit
            viewModel.search(lat,lon,ofs)
        })
    )

    @SuppressLint("MissingPermission")
    override fun binding() {
        dataBinding.adapter = adapter
        (dataBinding.recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        coroutinesLauncherWithCreatedState{
            viewModel.data.collect {
                it?.let {
                    if(it.results.isNotEmpty()){
                        Log.d("ASDA","ofs ${it.ofs}")
                        val proper = it.results.find {
                            it.poi!!.name!!.contains("Fadak")
                        }
                        proper?.let {s->
                            Log.d("ASDA 2","ofs ${it.ofs}")
                            Log.d("ASDA 2","ofs ${it.ofs}  ${s.poi!!.name}")
                        }
                        adapter.changeRangeList(it.results,it.ofs,it.ofs+it.limit)
                    }

                }
            }
        }

        coroutinesLauncherWithCreatedState{
            getPermissions(requireActivity(), arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION
            )).collect {
                if(it.granted){
                    val listener = LocationListener {location->
                        if(diffInTwoPos(lat,location.latitude,lon,location.longitude) > 100f){
                            adapter.clearList()
                            ofs = 0
                        }
                        lat = location.latitude
                        lon = location.longitude

                        viewModel.search(lat,lon,ofs)
                    }
                    val locationManager =
                        requireActivity().applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10_000L, 100f, listener)

                }else{
                    requireContext().toast("Permission is not granted")
                }
            }
        }
    }
}
package ir.sass.ui.detail

import androidx.navigation.fragment.navArgs
import ir.sass.basedata.model.toReal
import ir.sass.baseui.view.MotherFragmentWithOutViewModel
import ir.sass.ui.R
import ir.sass.ui.databinding.FragmentNearbyPlaceDetailBinding

class NearByPlaceDetailFragment : MotherFragmentWithOutViewModel<FragmentNearbyPlaceDetailBinding>(
    MotherFragmentSetting(
        R.layout.fragment_nearby_place_detail,
        "detail"
    )
) {
    private val args by navArgs<NearByPlaceDetailFragmentArgs>()

    override fun binding() {
       dataBinding.item = toReal(args.arg)
    }
}
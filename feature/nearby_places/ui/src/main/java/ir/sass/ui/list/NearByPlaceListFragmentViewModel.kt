package ir.sass.ui.list

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.sass.baseui.view.MotherFragmentViewModel
import ir.sass.domain.model.NearByLocModel
import ir.sass.domain.usecase.GetNearByLocFromLocalUseCase
import ir.sass.domain.usecase.GetNearByLocFromRemoteUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NearByPlaceListFragmentViewModel @Inject constructor(
    private val getNearByLocFromRemoteUseCase: GetNearByLocFromRemoteUseCase,
    private val getNearByLocFromLocalUseCase: GetNearByLocFromLocalUseCase
) : MotherFragmentViewModel() {

    private val _data : MutableStateFlow<NearByLocModel?> = MutableStateFlow(null)
    val data : StateFlow<NearByLocModel?> = _data
    val limit = 10
    private val radius = 10000


    fun search(lat : Double,lon : Double,ofs : Int){

        viewModelScope.launch {
            launch {
                action(getNearByLocFromLocalUseCase(
                    GetNearByLocFromLocalUseCase.GetNearByLocFromLocalUseCaseParam(
                        lat,lon,radius,limit,ofs
                    )
                ),true){
                    viewModelScope.launch {
                        _data.emit(it)
                    }
                }
            }

            launch {
                action(getNearByLocFromRemoteUseCase(
                    GetNearByLocFromRemoteUseCase.GetNearByLocFromRemoteUseCaseParam(
                        lat,lon,radius,limit,ofs
                    )
                ),true){
                    viewModelScope.launch {
                        _data.emit(it)
                    }
                }
            }

        }
    }


}
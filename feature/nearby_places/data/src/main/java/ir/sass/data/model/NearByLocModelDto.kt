package ir.sass.data.model

import com.google.gson.annotations.SerializedName
import ir.sass.basedomain.model.Mapper
import ir.sass.domain.model.*

data class NearByLocModelDto(
    @SerializedName("results")
    val results: List<ResultDto>,
    @SerializedName("summary")
    val summary: SummaryDto
) : Mapper<NearByLocModel>{
    override fun cast(): NearByLocModel = NearByLocModel(
        results.map { it.cast() },summary.cast()
    )
}

data class ResultDto(
    @SerializedName("address")
    val address: AddressDto?,
    @SerializedName("dist")
    val dist: Double?,
    @SerializedName("entryPoints")
    val entryPoints: List<EntryPointDto>?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("info")
    val info: String?,
    @SerializedName("poi")
    val poi: PoiDto?,
    @SerializedName("position")
    val position: PosDto?,
    @SerializedName("score")
    val score: Double?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("viewport")
    val viewport: ViewportDto?
) : Mapper<Result>{
    override fun cast(): Result = Result(
        address?.cast(),dist,entryPoints?.map { it.cast() },id,info,poi?.cast(),position?.cast(),score,type,viewport?.cast()
    )
}

data class SummaryDto(
    @SerializedName("fuzzyLevel")
    val fuzzyLevel: Int,
    @SerializedName("geoBias")
    val geoBias: PosDto,
    @SerializedName("numResults")
    val numResults: Int,
    @SerializedName("offset")
    val offset: Int,
    @SerializedName("queryTime")
    val queryTime: Int,
    @SerializedName("queryType")
    val queryType: String,
    @SerializedName("totalResults")
    val totalResults: Int
) : Mapper<Summary>{
    override fun cast(): Summary = Summary(
        fuzzyLevel,geoBias.cast(),numResults,offset,queryTime,queryType,totalResults
    )
}

data class AddressDto(
    @SerializedName("country")
    val country: String?,
    @SerializedName("countryCode")
    val countryCode: String?,
    @SerializedName("countryCodeISO3")
    val countryCodeISO3: String?,
    @SerializedName("countrySecondarySubdivision")
    val countrySecondarySubdivision: String?,
    @SerializedName("countrySubdivision")
    val countrySubdivision: String?,
    @SerializedName("freeformAddress")
    val freeformAddress: String?,
    @SerializedName("localName")
    val localName: String?,
    @SerializedName("municipality")
    val municipality: String?,
    @SerializedName("postalCode")
    val postalCode: String?,
    @SerializedName("streetName")
    val streetName: String?,
    @SerializedName("streetNumber")
    val streetNumber: String?
) : Mapper<Address>{
    override fun cast(): Address = Address(
        country,countryCode,countryCodeISO3,countrySecondarySubdivision,countrySubdivision,freeformAddress,
        localName,municipality,postalCode,streetName,streetNumber
    )
}

data class EntryPointDto(
    @SerializedName("position")
    val position: PosDto?,
    @SerializedName("type")
    val type: String?
) : Mapper<EntryPoint>{
    override fun cast(): EntryPoint = EntryPoint(
        position?.cast(),type
    )
}


data class PoiDto(
    @SerializedName("categories")
    val categories: List<String>?,
    @SerializedName("categorySet")
    val categorySet: List<CategorySetDto>?,
    @SerializedName("classifications")
    val classifications: List<ClassificationDto>?,
    @SerializedName("name")
    val name: String?
) : Mapper<Poi>{
    override fun cast(): Poi = Poi(
        categories,categorySet?.map { it.cast() },classifications?.map {it.cast()},name
    )
}


data class ViewportDto(
    @SerializedName("btmRightPoint")
    val btmRightPoint: PosDto?,
    @SerializedName("topLeftPoint")
    val topLeftPoint: PosDto?
) : Mapper<Viewport>{
    override fun cast(): Viewport = Viewport(
        btmRightPoint?.cast(),topLeftPoint?.cast()
    )
}


data class CategorySetDto(
    @SerializedName("id")
    val id: Int?
) : Mapper<CategorySet>{
    override fun cast(): CategorySet = CategorySet(id)
}

data class ClassificationDto(
    @SerializedName("code")
    val code: String?,
    @SerializedName("names")
    val names: List<NameDto>?
) : Mapper<Classification>{
    override fun cast(): Classification = Classification(
        code,names?.map { it.cast() }
    )
}

data class NameDto(
    @SerializedName("name")
    val name: String?,
    @SerializedName("nameLocale")
    val nameLocale: String?
) : Mapper<Name>{
    override fun cast(): Name = Name(
        name,nameLocale
    )
}



data class PosDto(
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lon")
    val lon: Double?
) : Mapper<Pos> {
    override fun cast(): Pos = Pos(
        lat,lon
    )
}


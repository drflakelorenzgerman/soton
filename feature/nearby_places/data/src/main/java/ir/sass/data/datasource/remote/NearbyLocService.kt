package ir.sass.data.datasource.remote

import ir.sass.data.model.NearByLocModelDto
import retrofit2.http.GET
import retrofit2.http.Query

interface NearbyLocService {
    @GET("search/2/nearbySearch/.json")
    suspend fun nearbySearch(
        @Query("lat") lat : Double,
        @Query("lon") lon : Double,
        @Query("radius") radius : Int,
        @Query("limit") limit : Int,
        @Query("ofs") ofs : Int
        ) : NearByLocModelDto
}


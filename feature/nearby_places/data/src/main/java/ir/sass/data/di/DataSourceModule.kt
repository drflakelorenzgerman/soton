package ir.sass.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.sass.data.datasource.remote.NearbyLocService
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {
    @Provides
    fun provideNearByLocWebService(retrofit: Retrofit) : NearbyLocService =
        retrofit.create(NearbyLocService::class.java)
}
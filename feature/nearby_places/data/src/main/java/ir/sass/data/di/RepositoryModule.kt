package ir.sass.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.sass.data.repository.NearbyLocRepositoryImp
import ir.sass.domain.repository.NearbyLocRepository

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun bindNearbyLocRepositoryImp (repo : NearbyLocRepositoryImp) : NearbyLocRepository
}
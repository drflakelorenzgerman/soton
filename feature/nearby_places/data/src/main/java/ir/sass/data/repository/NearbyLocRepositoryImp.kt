package ir.sass.data.repository

import android.util.Log
import ir.sass.basedata.db.NearbyLocDao
import ir.sass.basedata.db.model.LastPosModelEntity
import ir.sass.basedata.db.model.NearByLocResultModelEntity
import ir.sass.basedata.db.model.NearByLocSummeryModelEntity
import ir.sass.basedata.model.safeApi
import ir.sass.basedata.model.toJsonString
import ir.sass.basedata.model.toReal
import ir.sass.basedata.util.diffInTwoPos
import ir.sass.basedomain.model.Domain
import ir.sass.data.datasource.remote.NearbyLocService
import ir.sass.domain.model.NearByLocModel
import ir.sass.domain.model.Pos
import ir.sass.domain.model.Summary
import ir.sass.domain.repository.NearbyLocRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class NearbyLocRepositoryImp @Inject constructor(
    val nearbyLocDao: NearbyLocDao,
    val nearbyLocService: NearbyLocService
) : NearbyLocRepository {
    private val lock = mutableMapOf<Int,Boolean>()

    override fun nearbySearch(
        lat: Double,
        lon: Double,
        radius: Int,
        limit: Int,
        ofs: Int
    ): Flow<Domain<NearByLocModel>> = flow<Domain<NearByLocModel>> {
        emit(Domain.Progress<NearByLocModel>())

        nearbyLocDao.getRangeOfNearByLocResult(limit,ofs).collect {
            val isLock = lock[ofs]
            if(isLock == null || !isLock){
                lock[ofs] = false
                val converted = it.map {
                    toReal<ir.sass.domain.model.Result>(it.results)
                }
                val summery =nearbyLocDao.getSummery()
                val summeryConverted = if(summery == null) null else toReal<Summary>(summery.summery)
                emit(Domain.Result(Result.success(NearByLocModel(converted,summeryConverted,limit,ofs))))
            }
        }
    }.flowOn(IO)


    private var deleteLastLocForOnce = true
    override fun nearbySearchRemote(
        lat: Double,
        lon: Double,
        radius: Int,
        limit: Int,
        ofs: Int
    ): Flow<Domain<NearByLocModel>> = flow {
        val lastPos = nearbyLocDao.getLastLocation()
        val dis = if(lastPos != null) diffInTwoPos(lastPos.lat,lat,lastPos.lon,lon) else 101f
        if(/*dis > 100f*/true){
            lock[ofs] = false
            nearbyLocDao.deleteLastLocation()
            nearbyLocDao.insertLastLocation(LastPosModelEntity(lat,lon))
            emit(Domain.Progress<NearByLocModel>())
            val result = safeApi { nearbyLocService.nearbySearch(lat,lon,radius,limit,ofs) }
            if(result.isSuccess){
                result.getOrNull()?.let {
                    if(deleteLastLocForOnce){
                        deleteLastLocForOnce = false
                        nearbyLocDao.deleteNearByLocResult()
                    }
                    nearbyLocDao.deleteSummrey()
                    nearbyLocDao.insertSummery(NearByLocSummeryModelEntity(summery = toJsonString(it.summary)))
                    val converted = it.results.map {
                        NearByLocResultModelEntity(results = toJsonString(it))
                    }
                    nearbyLocDao.insertNearByLocResults(converted)
                }
            }else{
                emit(Domain.Result(result)) // error
            }
        }
    }.flowOn(IO)



}